import app, { startServer, shutdownServer } from '../src/index';
import request from 'supertest';

beforeAll(async () => {
  await startServer();
});

afterAll(async () => {
  await shutdownServer();
});

describe('Express App', () => {
  it('should respond with "Express + TypeScript Server" message on root path', async () => {
    const response = await request(app).get('/');
    expect(response.status).toBe(200);
    expect(response.text).toContain('Express + TypeScript Server');
  });

  it('should return 404 Not Found on non-existent path', async () => {
    const response = await request(app).get('/non-existent');
    expect(response.status).toBe(404);
  });
});
