import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;
app.get('/', (req: Request, res: Response) => {
  res.send(`Express + TypeScript Server`);
});

const server = app.listen(port, () => {
  console.log(`[Server]: Server is running at http://localhost:${port}`);
});

const startServer = async () => {
  return new Promise<void>((resolve) => {
    server.on('listening', () => {
      console.log('[Server]: Server started');
      resolve();
    });
  });
};

const shutdownServer = async () => {
  return new Promise<void>((resolve) => {
    server.close(() => {
      console.log('[Server]: Server closed');
      resolve();
    });
  });
};

export { startServer, shutdownServer };

export default app;
