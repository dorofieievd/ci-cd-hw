"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.shutdownServer = exports.startServer = void 0;
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const app = (0, express_1.default)();
const port = process.env.PORT;
app.get('/', (req, res) => {
    res.send(`Express + TypeScript Server`);
});
const server = app.listen(port, () => {
    console.log(`[Server]: Server is running at http://localhost:${port}`);
});
const startServer = () => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve) => {
        server.on('listening', () => {
            console.log('[Server]: Server started');
            resolve();
        });
    });
});
exports.startServer = startServer;
const shutdownServer = () => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve) => {
        server.close(() => {
            console.log('[Server]: Server closed');
            resolve();
        });
    });
});
exports.shutdownServer = shutdownServer;
exports.default = app;
